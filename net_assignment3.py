import sys

class network:
    def __init__(self):
        pass


    def run(self):
        while True:
            command = input().lower()
            try:
                if command.startswith('sec '):
                    parsed_cmd = command.split()
                    assert len(parsed_cmd) == 2
                    assert parsed_cmd[-1].isnumeric()
                    t = int(parsed_cmd[-1])
                    #TODO go forward in time

                elif command.startswith('add router '):
                    parsed_cmd = command.split()
                    assert len(parsed_cmd) == 3
                    assert parsed_cmd[-1].isnumeric()
                    router_num = int(parsed_cmd[-1])
                    # TODO add router with unique number check

                elif command.startswith('connect '):
                    parsed_cmd = command.split()
                    assert len(parsed_cmd) == 4
                    assert parsed_cmd[1].isnumeric()
                    r1_num = int(parsed_cmd[1])
                    assert parsed_cmd[2].isnumeric()
                    r2_num = int(parsed_cmd[2])
                    assert parsed_cmd[3].isnumeric()
                    link_cost = int(parsed_cmd[3])
                    # TODO connect 2 routers

                elif command.startswith('link '):
                    parsed_cmd = command.split()
                    assert len(parsed_cmd) == 4
                    assert parsed_cmd[1].isnumeric()
                    r1_num = int(parsed_cmd[1])
                    assert parsed_cmd[2].isnumeric()
                    r2_num = int(parsed_cmd[2])

                    if parsed_cmd[3] == 'd':
                        # TODO disconnect link
                        pass
                    elif parsed_cmd[3] == 'e':
                        # TODO reconnect link
                        pass
                    else:
                        raise ValueError('link argument should be e/d')

                elif command.startswith('ping '):
                    parsed_cmd = command.split()
                    assert len(parsed_cmd) == 3
                    assert parsed_cmd[1].isnumeric()
                    r1_num = int(parsed_cmd[1])
                    assert parsed_cmd[2].isnumeric()
                    r2_num = int(parsed_cmd[2])
                    # TODO r1 ping r2
                    pass

                elif command.startswith('monitor '):
                    parsed_cmd = command.split()
                    assert len(parsed_cmd) == 2
                    if parsed_cmd[1] == 'e':
                        # TODO monitor on
                        pass
                    elif parsed_cmd[1] == 'd':
                        #TODO monitor off
                        pass
                    else:
                        raise ValueError('monitor arg must be e/d')

                else:
                    raise ValueError('invalid command: ' + command)
            except Exception as err:
                sys.stderr.write(str(err))


if __name__ == "__main__":
    net = network()
    net.run()
